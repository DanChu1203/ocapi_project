const app = require('express')();
const https = require('https');
const request = require('request');
const url = require('url');
const jsonParser = require("json-parser");

var categoryList = {};

app.set('view engine', 'ejs');

//Index route
app.get('/', function (req, res) {
	options = {
		protocol: 'http:',
		hostname: 'lyons5.evaluation.dw.demandware.net',
		pathname: '/s/SiteGenesis/dw/shop/v17_4/categories/root',
		query: {levels : 1, client_id : 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'}
	}

	var reqURL = url.format(options);

	//Request for root categories
	request.get(reqURL, function(error, response, body){

		var jsonObject = jsonParser.parse(response.body, null, true);
		
		res.render('index.ejs', {categories: jsonObject['categories'], categoriesLength: jsonObject['categories'].length});
	});
});

//Show
app.get('/categories/:category', function(req, res){
	options = {
		protocol: 'http:',
		hostname: 'lyons5.evaluation.dw.demandware.net',
		pathname: '/s/SiteGenesis/dw/shop/v17_4/categories/' + req.params.category,
		query: {levels : 2, client_id : 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'}
	};

	var reqURL = url.format(options);

	request.get(reqURL, function(error, response, body){

		var jsonObject = jsonParser.parse(response.body, null, true);
		
		res.render('show.ejs', {categories: jsonObject});
	});

});

app.get('/products/:product', function(req, res){
	options = {
		protocol: 'http:',
		hostname: 'lyons5.evaluation.dw.demandware.net',
		pathname: '/s/SiteGenesis//dw/shop/v17_4/product_search',
		query: {q: req.params.product, expand: 'images', client_id: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'}
	};

	var reqURL = url.format(options);

	request.get(reqURL, function(error, response, body){

		var jsonObject = jsonParser.parse(response.body, null, true);

		res.render('products.ejs', {products: jsonObject});

	});
});

app.get('/products/:product/:start', function(req, res){
	options = {
		protocol: 'http:',
		hostname: 'lyons5.evaluation.dw.demandware.net',
		pathname: '/s/SiteGenesis//dw/shop/v17_4/product_search',
		query: {q: req.params.product, start: req.params.start, expand: 'images', client_id: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'}
	};

	var reqURL = url.format(options);

	request.get(reqURL, function(error, response, body){

		var jsonObject = jsonParser.parse(response.body, null, true);

		res.render('products.ejs', {products: jsonObject});
	});
});

app.get('/product_show/:product/:product_id', function(req, res){
	options = {
		protocol: 'http:',
		hostname: 'lyons5.evaluation.dw.demandware.net',
		pathname: '/s/SiteGenesis//dw/shop/v17_4/products/' + req.params.product_id,
		query: {q: req.params.product, expand: 'images,prices', client_id: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'}
	};

	var reqURL = url.format(options);

	request.get(reqURL, function(error, response, body){

		var jsonObject = jsonParser.parse(response.body, null, true);
		console.log(jsonObject);

		res.render('productShow.ejs', {product: jsonObject});

	});
});

app.listen(3000,function(){
	console.log("Server started port 3000");
});
// var setCategories = function(jsonObject){
// 	var jsonObjectLength = jsonObject.length;

// 	for(var i = 0; i < jsonObjectLength; i++){
// 		categoryList[jsonObject[i].id] = [];
// 	}

// 	return categoryList;
// };

// var setSubCategories = function(jsonObject){
// 	var jsonObjectLength = jsonObject.length;

// 	//Unpacks the data array
// 	for(var i = 0; i < jsonObjectLength; i++){
// 		if(jsonObject[i]['categories']){
// 			var subCategoryLength = jsonObject[i]['categories'].length;

// 			//Unpacks the categories array in data
// 			for(var j = 0; j < subCategoryLength; j++){
// 				var hashKey = jsonObject[i]['categories'][j].id;
// 				var hashToPush = {};
// 				hashToPush[hashKey] = [];
// 				categoryList[jsonObject[i].id].push(hashToPush);


// 				if(jsonObject[i]['categories'][j]['categories']){
// 					var subSubCategoryLength = jsonObject[i]['categories'][j]['categories'].length;

// 					//Unpacks the categories in categories
// 					for(var k = 0; k < subSubCategoryLength; k++){
// 						categoryList[jsonObject[i].id][j][hashKey].push(jsonObject[i]['categories'][j]['categories'][k].id);
// 					}
// 				}
// 			}
// 		}
// 	}
// };

// var createQueryString = function(categoryKeys){
// 	var categoryKeysLength = categoryKeys.length;
// 	var queryString = '';

// 	for(var i = 0; i < categoryKeysLength; i++){
// 		queryString = queryString + categoryKeys[i] + ',';
// 	}

// 	return queryString;
// };

//Set app to listen on port 3000

// jsonObj['data'][0]['categories'][0]['categories'][0].id